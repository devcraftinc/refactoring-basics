// Copyright 2018-2018 DevCraft, Inc
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


package sessions;

import java.util.*;

public class Vector2 {
    public Vector2(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public final double x;
    public final double y;

    public Vector2 perpendicular() {
        return new Vector2(-y, x);
    }

    public Vector2 normalize() {
        if (Objects.equals(this, getZero()))
            return this;

        return divide(magnitude());
    }

    public Vector2 divide(double denominator) {
        return new Vector2(x / denominator, y / denominator);
    }

    public Vector2 multiply(double scale) {
        return new Vector2(x * scale, y * scale);
    }

    public double magnitude() {
        return Math.sqrt(x * x + y * y);
    }

    public double dot(Vector2 other) {
        return x * other.y - y * other.x;
    }

    public static Vector2 getZero() {
        return new Vector2(0, 0);
    }

    public Vector2 withX(double x) {
        return new Vector2(x, y);
    }

    public Vector2 withY(double y) {
        return new Vector2(x, y);
    }

    public Vector2 plus(Vector2 rhs) {
        return new Vector2(x + rhs.x, y + rhs.y);
    }

    public Vector2 minus(Vector2 rhs) {
        return new Vector2(x - rhs.x, y - rhs.y);
    }

    public Vector2 negate() {
        return getZero().minus(this);
    }
}

