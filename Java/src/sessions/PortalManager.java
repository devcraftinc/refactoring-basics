// Copyright 2018-2018 DevCraft, Inc
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


package sessions;

public class PortalManager {
    public static void collideWithPortal(Portal portal, GameObject colliding) {
        switch (portal.getType()) {
            case BlackHole: {
                // we don't know where it is going but we know it's not here anymore...
                colliding.Destroy();
                break;
            }
            case WhiteHole: {
                // reflect off the line represented by the white hole's normal
                Vector2 n = portal.getForward().normalize();
                colliding.Velocity(colliding.getVelocity().minus(n.multiply(2 * colliding.getVelocity().dot(n))));
                break;
            }
            case TwoWay: {
                // if the object is traveling forward, relative to the portal, don't act on the collision
                if (portal.getForward().normalize().perpendicular().dot(colliding.getVelocity()) > 0)
                    break;
                // teleport to the portal's position
                colliding.setPosition(portal.getPartner().getPosition());
                // get angle between portal and other rotated 180 degrees
                double angle = Math.PI + Math.acos(portal.getForward().normalize().dot(portal.getPartner().getForward().normalize()));
                // rotate
                colliding.Velocity(new Vector2(colliding.getVelocity().x * Math.cos(angle), colliding.getVelocity().y * Math.sin(angle)));
                break;
            }
        }
    }
}

