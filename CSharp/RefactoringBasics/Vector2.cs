﻿// Copyright 2018-2018 DevCraft, Inc
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;

namespace RefactoringBasics
{
  public struct Vector2
  {
    public Vector2(double x, double y)
    {
      this.x = x;
      this.y = y;
    }

    public readonly double x;
    public readonly double y;

    public Vector2 Perpendicular()
    {
      return new Vector2(-y, x);
    }

    public Vector2 Normalize()
    {
      if (Equals(this, GetZero()))
        return this;

      return Divide(Magnitude());
    }

    public Vector2 Divide(double denominator)
    {
      return new Vector2(x / denominator, y / denominator);
    }

    public Vector2 Multiply(double scale)
    {
      return new Vector2(x * scale, y * scale);
    }

    public double Magnitude()
    {
      return Math.Sqrt(x * x + y * y);
    }

    public double Dot(Vector2 other)
    {
      return x * other.y - y * other.x;
    }

    public static Vector2 GetZero()
    {
      return new Vector2(0, 0);
    }

    public Vector2 WithX(double x)
    {
      return new Vector2(x, y);
    }

    public Vector2 WithY(double y)
    {
      return new Vector2(x, y);
    }

    public Vector2 Plus(Vector2 rhs)
    {
      return new Vector2(x + rhs.x, y + rhs.y);
    }

    public Vector2 Minus(Vector2 rhs)
    {
      return new Vector2(x - rhs.x, y - rhs.y);
    }

    public Vector2 Negate()
    {
      return GetZero().Minus(this);
    }
  }
}