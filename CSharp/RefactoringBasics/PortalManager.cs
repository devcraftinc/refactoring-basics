﻿// Copyright 2018-2018 DevCraft, Inc
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;

namespace RefactoringBasics
{
  public class PortalManager
  {
    public static void CollideWithPortal(Portal portal, GameObject colliding)
    {
      switch (portal.Type)
      {
        case Portal.PortalType.BlackHole:
        {
          // we don't know where it is going but we know it's not here anymore...
          colliding.Destroy();
          break;
        }
        case Portal.PortalType.WhiteHole:
        {
          // reflect off the line represented by the white hole's normal
          var n = portal.Forward.Normalize();
          colliding.Velocity = colliding.Velocity.Minus(n.Multiply(2 * colliding.Velocity.Dot(n)));
          break;
        }
        case Portal.PortalType.TwoWay:
        {
          // if the object is traveling forward, relative to the portal, don't act on the collision
          if (portal.Forward.Normalize().Perpendicular().Dot(colliding.Velocity) > 0)
            break;
          // teleport to the portal's position
          colliding.Position = portal.Partner.Position;
          // get angle between portal and other rotated 180 degrees
          var angle = Math.PI + Math.Acos(portal.Forward.Normalize().Dot(portal.Partner.Forward.Normalize()));
          // rotate
          colliding.Velocity =
            new Vector2(colliding.Velocity.x * Math.Cos(angle), colliding.Velocity.y * Math.Sin(angle));
          break;
        }
      }
    }
  }
}